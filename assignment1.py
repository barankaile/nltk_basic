#!/usr/bin/env python
import nltk, zipfile, argparse, sys
from nltk import *
##########################################################################
#####
## Utility Functions 
##########################################################
##########################################################################
#####
# This method takes the path to a zip archive.
# It first creates a ZipFile object.
# Using a list comprehension it creates a list where each element contains
# the raw text of the fable file.
# We iterate over each named file in the archive:
#     for fn in zip_archive.namelist()
# For each file that ends with '.txt' we open the file in read only
# mode:
#     zip_archive.open(fn, 'rU')
# Finally, we read the raw contents of the file:
#     zip_archive.open(fn, 'rU').read()
#Kaile Baran (kbaran@ucsc.edu).Submission for assignment 1
def unzip_corpus(input_file):
    zip_archive = zipfile.ZipFile(input_file)
    try:
        contents = [zip_archive.open(fn, 'rU').read().decode('utf-8')
                for fn in zip_archive.namelist() if fn.endswith(".txt")]
    except ValueError as e:
        contents = [zip_archive.open(fn, 'r').read().decode('utf-8')
                for fn in zip_archive.namelist() if fn.endswith(".txt")]
    return contents

# Stub Functions 
def process_corpus(corpus_name):
    input_file = corpus_name + ".zip"
    corpus_contents = unzip_corpus(input_file)
    sentence_tokenized = []
    
    for idx, item in enumerate(corpus_contents):
        corpus_contents[idx] = sent_tokenize(item)
    for idx, item in enumerate(corpus_contents):
        for idy, sentence in enumerate(item):
            item[idy] = word_tokenize(sentence)
    word_count = 0
    for item in corpus_contents:
        for sentence in item:
            word_count += len(sentence)
    untagged = []
    for blog in corpus_contents:
        for sentence in blog:
            for word in sentence:
                untagged.append(word)
    lowercase_tagged = []     
    for idx,blog in enumerate(corpus_contents):
        for idy, sentence in enumerate(blog):
            blog[idy] = pos_tag(sentence)
            lowercase_tagged += pos_tag(sentence)

    
    for idx, pair in enumerate(lowercase_tagged):
           lowercase_tagged[idx] = lowercase_tagged[idx][::-1]
           lowercase_tagged[idx] = list(lowercase_tagged[idx])
           lowercase_tagged[idx] = tuple([lowercase_tagged[idx][0], lowercase_tagged[idx][1].lower()])
    nn = []
    vbd = []
    jj = []
    rb = []
    for word in lowercase_tagged:
        if word[0] == "NN":
            nn.append(word)
        elif word[0] == "VBD":
            vbd.append(word)
        elif word[0] == "JJ":
            jj.append(word)
        elif word[0] == "RB":
            rb.append(word)
    frequent_nn = FreqDist(nn).most_common(1)
    frequent_vbd = FreqDist(vbd).most_common(1)
    frequent_jj = FreqDist(jj).most_common(1)
    frequent_rb = FreqDist(rb).most_common(1)
        
    sys.stdout = open(corpus_name + "-pos-word-freq.txt",'w')
    cfdist = nltk.ConditionalFreqDist(lowercase_tagged)
    for item in cfdist:
        print(item)
    #cfdist.tabulate()
    sys.stdout = sys.__stdout__
    
    
    
    pos_list = []
    for blog in corpus_contents:
        for sentence in blog:
                pos_list += sentence
    
    pos_counts = nltk.FreqDist(tag for (word, tag) in pos_list)
    l_words = [w.lower() for w in untagged]    
    vocab = sorted(set(l_words))
    fdist = FreqDist(l_words)
    
    m_common = fdist.most_common()
    for idx, blog in enumerate(corpus_contents):
        for idy, sentence in enumerate(blog):
            for idz, pos_word in enumerate(sentence):
                sentence[idz] = '/'.join(pos_word)
    
    pos_filename = corpus_name + "-pos.txt"
    f = open(pos_filename, 'w')
    
    for blog in corpus_contents:
        for sentence in blog:
            temp = " ".join(sentence)
            f.write(temp+ "\n")
        f.write("\n")
    
    f.close()
    sys.stdout = open(corpus_name + "-word-freq.txt",'w')
    print(m_common)
    sys.stdout = sys.__stdout__
    
    total_corpus = []
    for word in l_words:
        total_corpus.append(word)
    total_corpus = Text(total_corpus)
        
    # Your code goes here
    print("1. Corpus name: ", corpus_name)
    print("2. Total words in corpus: ", word_count)
    print("3. Vocabulary size of corpus: ", len(vocab))
    common_pos = pos_counts.most_common(1)
    print("4. The most frequent part-of-speech tag is", common_pos[0][0], "with frequency",common_pos[0][1]) #Change this to calculate frequency relative to entire corpus    
    print("5a. The most frequent word in the POS", frequent_nn[0][0][0], "is", frequent_nn[0][0][1], "and its similar words are:", end='')
    total_corpus.similar(frequent_nn[0][0][1])
    print("5b. The most frequent word in the POS", frequent_vbd[0][0][0], "is", frequent_vbd[0][0][1], "and its similar words are:", end='') 
    total_corpus.similar(frequent_vbd[0][0][1])
    print("5c. The most frequent word in the POS", frequent_jj[0][0][0], "is", frequent_jj[0][0][1], "and its similar words are:",end='') 
    total_corpus.similar(frequent_jj[0][0][1])
    print("5d. The most frequent word in the POS", frequent_rb[0][0][0], "is", frequent_rb[0][0][1], "and its similar words are:",end='') 
    total_corpus.similar(frequent_rb[0][0][1])
    print("6. Collocations:", end='')
    total_corpus.collocations()
    
    
    
    
    pass

# Program Entry Point 
#python3 assignment1.py --corpus blogs
#python3 assignment1.py --corpus fables

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Assignment 1')
    parser.add_argument('--corpus', required=True, dest="corpus", 
        metavar='NAME', help='Which corpus to process {fables, blogs}')
    args = parser.parse_args()
    corpus_name = args.corpus
    if corpus_name == "fables" or "blogs":
        process_corpus(corpus_name)
    else:
        print("Unknown corpus name: {0}".format(corpus_name))